<?php
require 'libs/phpQuery.php';             // подключение библиотеки phpQuery 
require 'libs/PHPExcel.php';             // подключение библиотеки PHPExcel 
require 'db.php';                        // подключение к БД + библиотеки RedBean

// Основной класс CurlQuery
class CurlQuery
{
    private static $error_codes = [
        "CURLE_UNSUPPORTED_PROTOCOL",
        "CURLE_FAILED_INIT",
        "CURLE_FTP_BAD_FILE_LIST",
        "CURLE_CHUNK_FAILED"
    ];

    public static function getPage($params = [])
    {

        if ($params) {

            if (!empty($params["url"])) {

                $url = $params["url"];

				//Прописываем браузер, имя которого отобразиться на удаленном сервере             
                $useragent = !empty($params["useragent"]) ? $params["useragent"] : "Mozilla/5.0 (Windows NT 6.3; W…) Gecko/20100101 Firefox/57.0";
				
				//Делей страницы
                $timeout = !empty($params["timeout"]) ? $params["timeout"] : 5;
                $connecttimeout = !empty($params["connecttimeout"]) ? $params["connecttimeout"] : 5;
                $head = !empty($params["head"]) ? $params["head"] : false;
				
				//Принимаем/Не принимаем cookie
                $cookie_file = !empty($params["cookie"]["file"]) ? $params["cookie"]["file"] : false;
                $cookie_session = !empty($params["cookie"]["session"]) ? $params["cookie"]["session"] : false;
				
				//Используем/Не используем прокси
                $proxy_ip = !empty($params["proxy"]["ip"]) ? $params["proxy"]["ip"] : false;
                $proxy_port = !empty($params["proxy"]["port"]) ? $params["proxy"]["port"] : false;
                $proxy_type = !empty($params["proxy"]["type"]) ? $params["proxy"]["type"] : false;
                $headers = !empty($params["headers"]) ? $params["headers"] : false;
                $post = !empty($params["post"]) ? $params["post"] : false;


                if ($cookie_file) {

                    file_put_contents(__DIR__ . "/" . $cookie_file, "");
                }
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLINFO_HEADER_OUT, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
                curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connecttimeout);
                if (strpos($url, "https") !== false) {

                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                }
                if ($cookie_file) {

                    curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__ . "/" . $cookie_file);
                    curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__ . "/" . $cookie_file);

                    if ($cookie_session) {

                        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
                    }
                }
                if ($proxy_ip && $proxy_port && $proxy_type) {

                    curl_setopt($ch, CURLOPT_PROXY, $proxy_ip . ":" . $proxy_port);
                    curl_setopt($ch, CURLOPT_PROXYTYPE, $proxy_type);
                }

                if ($headers) {

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                }

                if ($post) {

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                }
                $content = curl_exec($ch);
                $info = curl_getinfo($ch);
                $error = false;

                if ($content === false) {

                    $data = false;
                    $error["message"] = curl_error($ch);
                    $error["code"] = self::$error_codes[curl_errno($ch)];
                } else {

                    $data["content"] = $content;
                    $data["info"] = $info;
                }

                curl_close($ch);

                return [
                    "data" => $data,
                    "error" => $error
                ];

            }
        }

        return false;
    }
}

// Основной класс парсера
class Parser
{
    public static function commitBean($bean)
    {
        R::begin();
        try {
            $res = R::store($bean);
            R::commit();
        } catch (Exception $e) {
            R::rollback();
            $res = false;
        }
        return $res;
    }

    // Метод сбора основных категорий 
    public static function setMainCategories()
    {
        $html = CurlQuery::getPage(['url' => 'https://www.robik-music.com/']);
        $html_ua = CurlQuery::getPage(['url' => 'https://www.robik-music.com/ua']);

        if (!empty($html["data"]) || !empty($html_ua["data"])) {

            $content = $html["data"]["content"];

            phpQuery::newDocument($content);

            $categories = pq(".footer-catalog-nav")->find("li");

            $tmp = [];
            foreach ($categories as $key => $category) {
                $category = pq($category)->find("a");
                $parent = 0;

                $categoryUrl = trim($category->attr("href"));

                $tmp[$key] = [
                    "text" => trim($category->attr("title")),
                    "text_ua" => NULL,
                    "url" => pathinfo($categoryUrl, PATHINFO_BASENAME),
                    "parent" => $parent
                ];
            }

            $content_ua = $html_ua["data"]["content"];

            phpQuery::newDocument($content_ua);
            $categories_ua = pq(".footer-catalog-nav")->find("li");

            foreach ($categories_ua as $key => $category) {
                $category = pq($category)->find("a");

                $tmp[$key]["text_ua"] = trim($category->attr("title"));
            }
            //print_r($tmp);

            // Запись в БД
            foreach ($tmp as $data) {
                $categoryData = R::dispense('category');
                $categoryData->title = $data['text'];
                $categoryData->title_ua = $data['text_ua'];
                $categoryData->url = $data['url'];
                $categoryData->parent = $data['parent'];
                R::store($categoryData);
            }
            phpQuery::unloadDocuments();
        }
    }

	// Метод сбора подкатегорий
    public static function setSubCategories($url, $parentID, $lastID = null)
    {
        $data = [];
        $html = CurlQuery::getPage(['url' => $url]);
        $html_ua = CurlQuery::getPage(['url' => preg_replace("/com/", "$0/ua", $url)]);

        if (!empty($html["data"]) || !empty($html_ua["data"])) {
            $content = $html["data"]["content"];
            $content_ua = $html_ua["data"]["content"];

            phpQuery::newDocument($content);

            $categories = pq(".catalog-tree")->find("li");

            if ($categories) {
                $lastInsertID = $lastID;
                foreach ($categories as $key => $category) {
                    $key = $key + $lastInsertID + 1;
                    $category = pq($category);
                    if ($category->attr("aria-level") == '1') {
                        $parent = 1;
                    } else if ($category->attr("aria-level") == '2') {
                        $parent = 2;
                    } else if ($category->attr("aria-level") == '3') {
                        $parent = 3;
                    }

                    $categoryLink = pq($category)->find("a");
                    $categoryUrl = trim($categoryLink->attr("href"));

                    if ($categoryLink->attr("title")) {
                        $data[$key] = [
                            "text" => trim($categoryLink->attr("title")),
                            "url" => pathinfo($categoryUrl, PATHINFO_BASENAME),
                            "parent" => $parent,
                        ];
                    }
                }

                $lastInsertIDT = 0;
                foreach ($data as $index => $dataID) {

                    $dataID['id'] = $index;
                    if ($dataID['parent'] == '1') {
                        $dataID['parent'] = $parentID;
                        $lastInsertID = $index;
                    } elseif ($dataID['parent'] == '2') {
                        $dataID['parent'] = $lastInsertID;
                        $lastInsertIDT = $index;

                    } elseif ($dataID['parent'] == '3') {
                        $dataID['parent'] = $lastInsertIDT;

                    }
                    $data[$index]["parentTrue"] = $dataID['parent'];
                }
                print_r($data);
            }
            phpQuery::newDocument($content_ua);
            $categories_ua = pq(".catalog-tree")->find("li");
            foreach ($categories_ua as $key => $category) {
                $key = $key + $lastID + 1;
                $category = pq($category);
                $categoryLink = pq($category)->find("a");
                if ($categoryLink->attr("title")) {
                    $data[$key]["text_ua"] = trim($categoryLink->attr("title"));
                }
            }
            //print_r($data);
			
            //Запись в БД
            foreach ($data as $arr) {
                $categoryData = R::load('category', 0);

                $categoryData->title = $arr['text'];
                if (!empty($arr['text_ua'])) {
                    $categoryData->title_ua = $arr['text_ua'];
                } else {
                    $categoryData->title_ua = $arr['text'];
                }
                $categoryData->url = $arr['url'];
                $categoryData->parent = $arr['parentTrue'];
                R::store($categoryData);
            }

            phpQuery::unloadDocuments();
        }
    }

	// Метод парсинга товаров
    public function getProduct($url)
    {
        $html = CurlQuery::getPage([
            "url" => trim($url)
        ]);

        if (!empty($html["data"])) {
            $content = $html["data"]["content"];

            phpQuery::newDocument($content);

            //Начало пагинации
            $pagin = pq('.pagination')->find('li');
            $tmp = [];
            foreach ($pagin as $key => $category) {
                $category = pq($category)->find("a");

                if (($category->attr("title")) != 'вперед') {
                    $tmp[$key] = ["url" => trim($category->attr("href"))];
                }
            }

            // Получение кол-ва страниц пагинации
            $all_pages = preg_replace("/[^0-9]/", '', array_pop($tmp));

            // Создание массива и заполнение его ссылками пагинации
            $link = [];

            for ($i = 1; $i <= $all_pages['url']; $i++) {
                //for ($i = 1; $i <= 40; $i++) {
                $link[$i] = $url . '?p=' . $i;
            }

            //Сбор товаров по всем ссылкам из пагинации
            foreach ($link as $key => $elem) {

                $html_new = CurlQuery::getPage([
                    "url" => $elem
                ]);
                $content_new = $html_new["data"]["content"];

                phpQuery::newDocument($content_new);

                $products = pq(".main-content")->find(".item");
                $dataShort = [];
                $dataFull = [];
                foreach ($products as $key => $product) {

                    $product = pq($product);

                    $name = trim($product->find(".name-wrap")->text());
                    $modelName = explode(" ", $name);
                    $modelName = $modelName[count($modelName) - 1];
                    $dataShort[] = [
                        "name" => $name,
                        "modelname" => $modelName,
                        "url" => $productUrl = trim($product->find(".name-wrap a")->attr("href")),
                        "keyword" => pathinfo($productUrl, PATHINFO_BASENAME),
                        "art" => trim($product->find(".artno")->text()),
                        "price" => trim($product->find(".price")->text()),
                        "short_chars" => trim($product->find(".list-characteristics .short-characteristic-items")->html()),
                    ];

                    $countSlash = substr_count($dataShort[$key]["url"], '/');

                    //Парсинг полной части контента
                    $links = [trim($product->find(".name-wrap a")->attr("href"))];
                    //print_r($links);

                    foreach ($links as $k => $linkURL) {
                        $htmlFull = CurlQuery::getPage([
                            "url" => $linkURL
                        ]);

                        $fullContent = $htmlFull["data"]["content"];

                        phpQuery::newDocument($fullContent);

                        $fullContentProducts = pq(".main-content");
                        $fullContentLi= pq(".breadcrumb-wrap");

                        //$scr='<script>$.compare$.txt={"add2compare":"\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u043a \u0441\u0440\u0430\u0432\u043d\u0435\u043d\u0438\u044e","already_in_title":"\u0414\u043e\u0431\u0430\u0432\u043b\u0435\u043d \u043a \u0441\u0440\u0430\u0432\u043d\u0435\u043d\u0438\u044e","show_all_k_ewc":"\u0412 \u0441\u043f\u0438\u0441\u043a\u0435 {#k#} \u0442\u043e\u0432\u0430\u0440{#ewc#}. \u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u0432\u0441\u0435","ewc":",\u0430,\u043e\u0432"};$.compare$.url="https://www.robik-music.com/products/compare";$.wishlist$.url="https://www.robik-music.com/account/wishlist";$.wishlist$.txt={"confirm_delete":"\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0437\u0430\u043a\u043b\u0430\u0434\u043a\u0443?","confirm_delete_item":"\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0442\u043e\u0432\u0430\u0440 \u0438\u0437 \u0437\u0430\u043a\u043b\u0430\u0434\u043a\u0438?"};$.wishlist$.items=null;$.wishlist$.url="https://www.robik-music.com/account/wishlist";$.wishlist$.txt={"confirm_delete":"\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0437\u0430\u043a\u043b\u0430\u0434\u043a\u0443?","confirm_delete_item":"\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0442\u043e\u0432\u0430\u0440 \u0438\u0437 \u0437\u0430\u043a\u043b\u0430\u0434\u043a\u0438?"};$.wishlist$.items=null;var type=0,allow_retail=0;$.goods$.setUseIdPm(0);$.compare$.itemsList.id_catalog=165;$.compare$.itemsList.init();$.waitlist$.url="https://www.robik-music.com/account/waitlist";$.waitlist$.is_logged=0;$.waitlist$.txt={"confirm_delete_item":"\u0412\u044b \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u0442\u0435\u043b\u044c\u043d\u043e \u0445\u043e\u0442\u0438\u0442\u0435 \u0443\u0434\u0430\u043b\u0438\u0442\u044c \u0442\u043e\u0432\u0430\u0440 \u0441 \u043b\u0438\u0441\u0442\u0430 \u043e\u0436\u0438\u0434\u0430\u043d\u0438\u044f?","already_in":"\u0423\u0436\u0435 \u0432 \u043b\u0438\u0441\u0442e \u043e\u0436\u0438\u0434\u0430\u043d\u0438\u044f","already_in_title":"\u0423\u0436\u0435 \u0432 \u043b\u0438\u0441\u0442e \u043e\u0436\u0438\u0434\u0430\u043d\u0438\u044f"};$(document).ready(function(){$.compare$.markItemsAsActive();new $.good$({id_product:22174,id_pm:7300,pd:{"colors":[],"photos":[26629,45881],"mn":[],"img":{"tiles":{"wxh":"200x150\/","ext":".jpg"},"big":{"wxh":"370x370\/","ext":".jpg"},"zoom":{"wxh":"800x800\/","ext":".jpg"},"thumbs":{"wxh":"78x78\/","ext":".jpg"},"mobile_thumbs":{"wxh":"mobile-480x480\/","ext":".jpg"}},"img_path":"\/images\/product\/","media":{"45881":{"data":{"type":"photo"},"name":""},"26629":{"data":{"type":"photo"},"name":""}}},pm:{"7300":{"code":"22174","artno":"22174","price":{"number":"618.00","unit":"\u0433\u0440\u043d.","kind":false},"price_old":{"number":"0.00","unit":"\u0433\u0440\u043d.","kind":false},"balance":"1","id_balance":1,"photos":[],"modified":[]}},color_artno:[],prod_name:"Пюпитр HERCULES BS050B",mn_selected:{},id_n_color:0,balance:{"0":{"name":"\u041d\u0435\u0442 \u0432 \u043d\u0430\u043b\u0438\u0447\u0438\u0438","status":0,"price":1},"1":{"name":"\u0415\u0441\u0442\u044c \u0432 \u043d\u0430\u043b\u0438\u0447\u0438\u0438","status":1,"price":1},"2":{"name":"\u041f\u043e\u0434 \u0437\u0430\u043a\u0430\u0437","status":1,"price":1},"3":{"name":"\u041e\u0436\u0438\u0434\u0430\u0435\u0442\u0441\u044f","status":0,"price":1},"4":{"name":"\u0422\u043e\u0432\u0430\u0440 \u0437\u0430\u043a\u0430\u043d\u0447\u0438\u0432\u0430\u0435\u0442\u0441\u044f","status":1,"price":1},"6":{"name":"\u041f\u043e \u0437\u0430\u043f\u0440\u043e\u0441\u0443","status":0,"price":0},"9":{"name":"\u0421\u043d\u044f\u0442\u043e \u0441 \u043f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0430","status":0,"price":0}},useMedia:1});$.waitlist$.init()})</script>';

                        $dataFull[$key] = [
                            "category" => $fullContentProducts->find('.page-header .h1')->text(),
                            "category_main" => $fullContentLi->find('.breadcrumb li:eq(2)')->text(),
                            "img" => $imageLink = $fullContentProducts->find("div.sr-only img")->attr('src'),
                            "main_img" => 'catalog/product/' . pathinfo($imageLink, PATHINFO_FILENAME) . '.jpg',
                            //"h2" => trim($fullContentProducts->find(".description h1, h2")->text()),
                            "description" => trim($fullContentProducts->find(".description")->html()),
                            "good_chars" => trim($fullContentProducts->find(".characteristic-items")->html()),
                            "balance" => trim($fullContentProducts->find(".infobar .balance")->attr("data-status")),
                            "producer" => trim($fullContentProducts->find(".col-md-4.col-sm-6.hidden-xs .img-responsive")->attr("alt")),
                        ];


                        // Get keywords and sets parent id
                        $query = R::getAssoc('SELECT `id`, `title`, `url`, `parent` FROM category');

                        foreach ($query as $kk => $value) {

                            if ($countSlash == 5) {
                                //$keyWordM=trim(pathinfo(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_DIRNAME),PATHINFO_BASENAME));
                                $keyWordS = trim(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_BASENAME));


                                if (strpos($keyWordS, $value['url']) !== false) {
                                    if (trim($dataFull[$key]["category"]) == (trim($value['title']))) {
                                        $parentIdSS = $value['parent'] . ',' . $kk;
                                        $mainCat = $kk;
                                    }
                                }
                            } elseif ($countSlash == 6) {
                                $keyWordMain = trim(pathinfo(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_BASENAME));
                                $keyWordSub = trim(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_BASENAME));

                                    if (strpos($keyWordMain, $value['url']) !== false && trim($dataFull[$key]["category_main"]) == (trim($value['title']))) {
                                        $parentIdsMain = $value['parent'];
                                    } elseif (strpos($keyWordSub, $value['url']) !== false && (trim($dataFull[$key]["category"]) == trim($value['title']))) {
                                        $parentIdsSub = $value['parent'] . ',' . $kk;
                                        $mainCat = $kk;
                                    }
                            } elseif ($countSlash == 7) {
                                $keyWordSub3 = trim(pathinfo(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_BASENAME));
                                $keyWordSub4 = trim(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_BASENAME));
                                $keyWordSub2 = trim(pathinfo(pathinfo(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_BASENAME));
                                $keyWordMain2 = trim(pathinfo(pathinfo(pathinfo(pathinfo(pathinfo($linkURL, PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_DIRNAME), PATHINFO_BASENAME));

                                if (trim($dataFull[$kk]["category"]) == (trim($value['title']))) {
                                    if (strpos($keyWordMain2, $value['url']) !== false) {
                                        $parentIdsMain2 = $value['parent'];
                                    } elseif (strpos($keyWordSub2, $value['url']) !== false) {
                                        $parentIdsSub2 = $value['parent'];
                                    } elseif (strpos($keyWordSub3, $value['url']) !== false) {
                                        $parentIdsSub3 = $value['parent'];
                                    } elseif (strpos($keyWordSub4, $value['url']) !== false) {
                                        $parentIdsSub4 = $value['parent'] . ',' . $kk;
                                        $mainCat = $kk;
                                    }
                                }
                            }
                        }

                        if ($countSlash == 5) {
                            $parentIds = $parentIdSS;
                        } elseif ($countSlash == 6) {
                            $parentIds = $parentIdsMain . ',' . $parentIdsSub;
                        } elseif ($countSlash == 7) {
                            $parentIds = $parentIdsSub2 . ',' . $parentIdsSub3 . ',' . $parentIdsSub4;
                        }

                        $dataFull[$key]["parent"] = $parentIds;
                        $dataFull[$key]["maincat"] = $mainCat;

                        // Получение ID картинок и их скачивание
                        $queryScript = pq("script");

                        $pattern = '|\"photos\"\:\[(.+?)\]\,\"mn\"|';

                        preg_match_all($pattern, $queryScript, $matches);

                        foreach ($matches[1] as $k => $value) {
                            //print_r($value);
                            $dataFull[$key]["images"] = $value;
                        }

                        $imageId = explode(',', $dataFull[$key]["images"]);

                        $imgLinks = [];
                        foreach ($imageId as $l => $v) {
                            $imgLinks[] = 'https://www.robik-music.com/images/product/800x800/' . $v . '.jpg';
                        }

                        // Скачка картинок
                        //Parser::imageDownload($imgLinks);
                    }
                }
                // Запись в БД
                foreach ($dataShort as $index => $code) {
                    $productData = R::load('products', 0);
                    $productData->category = $dataFull[$index]['category'];
                    $productData->producer = $dataFull[$index]['producer'];
                    $productData->parent = $dataFull[$index]['parent'];
                    $productData->maincat = $dataFull[$index]['maincat'];
                    $productData->title = $code['name'];
                    $productData->url = $code['url'];
                    $productData->keyword = $code['keyword'];
                    $productData->art = $code['art'];
                    $productData->price = $code['price'];
                    $productData->short_chars = $code['short_chars'];

                    $productData->good_chars = $dataFull[$index]['good_chars'];
                    $productData->balance = $dataFull[$index]['balance'];
                    //$productData->img_link = $dataFull[$index]['img'];
                    $productData->main_img = $dataFull[$index]['main_img'];
                    $productData->img_name = $dataFull[$index]['images'];
                    $productData->description = $dataFull[$index]['description'];
                    $productData->modelname = $code['modelname'];
                    R::store($productData);
                }
                phpQuery::unloadDocuments();
            }
        }
    }

	// Метод скачивания картинок
    public function imageDownload()
    {

        $resArr = array();
        $resArr['errors'] = [];
        $resArr["img_info"] = [];
        $resArr["count_all"] = $resArr["count_ok"] = 0;

        $catalog_out_path = "images";
        if (!is_dir($catalog_out_path)) {
            mkdir($catalog_out_path, 0777, true);
        }

        $arrImg = R::getAssoc('SELECT `id`, `img_name` FROM products');

        $imgLinks = [];

        foreach ($arrImg as $l => $v) {
            $imgLinks[$l] = array_map(function ($el) {
                return 'https://www.robik-music.com/images/product/800x800/' . $el . '.jpg';
            }, explode(",", $v));

        }
        $imgTotal = call_user_func_array('array_merge', $imgLinks);

        $files1 = scandir($catalog_out_path);
        $files2 = array_diff($files1, array('.', '..'));
        //print_r($imgTotal);

        $superArr = [];
        $superArr2 = [];
        foreach ($files2 as $kkey => $fname) {
            $imgFname = (string)trim(pathinfo($fname, PATHINFO_FILENAME));
            $superArr[$kkey] = $imgFname;
        }

        foreach ($imgTotal as $key => $url) {
            $imgName = (string)trim(pathinfo($url, PATHINFO_FILENAME));
            $superArr2[$key] = $imgName;

            $html = CurlQuery::getPage($url);

            $imgContent = $html["data"]["content"];

            phpQuery::newDocument($imgContent);

            $image = file_get_contents($url);
            if (file_put_contents($catalog_out_path . "/" . $imgName . '.jpg', $image)) {
                $resArr["count_ok"]++;
                $resArr["img_info"][$key]["url"] = $url;
                $resArr["img_info"][$key]["local_url"] = $catalog_out_path . "/" . $imgName;
            } else {
                $resArr["img_info"][$key]["url"] = $url;
                $resArr["img_info"][$key]["local_url"] = "no_url";
                $resArr['errors'] = "Ошибка записи фото: " . $url;
            }
            $resArr["count_all"]++;

        }
        return $resArr;
    }

	// Метод трансформации в Exel
    public function sqlToExcel($data)
    {
        $phpexcel = new PHPExcel();
        $getDb = R::getAssoc('SELECT `id`, `id`, `producer`, `parent`, `maincat`, `title`, `keyword`, `art`, `price`, 
        `good_chars`, `balance`, `main_img` , `img_name` , `description`, `modelname` FROM ' . $data . ' LIMIT 1000 OFFSET 19000');

        $imageId = [];
        $i = 2;

        foreach ($getDb as $key => $arrDb) {
            $imageId[$key] = array_map(function ($el) {
                return 'catalog/product/' . $el . '.jpg';
            }, explode(",", $arrDb['img_name']));

            $getDb[$key]['imglist'] = $imageId;

            $wight = '0,0';
            $stockStatus = 5;

            $phpexcel->getActiveSheet()->setCellValueExplicit('A' . $i, strval($key), PHPExcel_Cell_DataType::TYPE_STRING);
            $string = $arrDb['title'];
            $phpexcel->getActiveSheet()->setCellValueExplicit('B' . $i, $string, PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('C' . $i, $arrDb['title'], PHPExcel_Cell_DataType::TYPE_STRING);
            $string3 = $arrDb['parent'];
            $phpexcel->getActiveSheet()->setCellValueExplicit('D' . $i, $string3, PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('E' . $i, strval($arrDb['maincat']), PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('F' . $i, $arrDb['art'], PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValue('M' . $i, 1);
            $phpexcel->getActiveSheet()->setCellValueExplicit('N' . $i, $arrDb['modelname'], PHPExcel_Cell_DataType::TYPE_STRING);
            $string4 = $arrDb['producer'];
            $phpexcel->getActiveSheet()->setCellValueExplicit('O' . $i, $string4, PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('P' . $i, $arrDb['main_img'], PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('Q' . $i, 'yes', PHPExcel_Cell_DataType::TYPE_STRING);
            $string5 = $arrDb['price'];
            $phpexcel->getActiveSheet()->setCellValueExplicit('R' . $i, $string5, PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValue('S' . $i, 0);
            $phpexcel->getActiveSheet()->setCellValueExplicit('T' . $i, date("Y-m-d H:i:s"), PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('U' . $i, date("Y-m-d H:i:s"), PHPExcel_Cell_DataType::TYPE_STRING);
            //$phpexcel->getActiveSheet()->setCellValueExplicit('W' . $i, $wight, PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('X' . $i, 'кг', PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValue('Y' . $i, 0);
            $phpexcel->getActiveSheet()->setCellValue('Z' . $i, 0);
            $phpexcel->getActiveSheet()->setCellValue('AA' . $i, 0);
            $phpexcel->getActiveSheet()->setCellValueExplicit('AB' . $i, 'см', PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('AC' . $i, 'true', PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValue('AD' . $i, 0);
            $phpexcel->getActiveSheet()->setCellValue('AP' . $i, $stockStatus);
            $phpexcel->getActiveSheet()->setCellValue('AQ' . $i, 0);
            $phpexcel->getActiveSheet()->setCellValueExplicit('AR' . $i, '0:', PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('AE' . $i, $arrDb['keyword'], PHPExcel_Cell_DataType::TYPE_STRING);
            $string6 = $arrDb['description'];
            $phpexcel->getActiveSheet()->setCellValueExplicit('AF' . $i, $string6, PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValueExplicit('AG' . $i, $arrDb['description'], PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValue('AV' . $i, 1);
            $phpexcel->getActiveSheet()->setCellValueExplicit('AW' . $i, 'true', PHPExcel_Cell_DataType::TYPE_STRING);
            $phpexcel->getActiveSheet()->setCellValue('AX' . $i, 1);

            $page = $phpexcel->setActiveSheetIndex();
            $page->setTitle('111KASD');
            $objWriter = PHPExcel_IOFactory::CreateWriter($phpexcel, 'Excel2007');
            $filename = '111KASD.xlsx';
            $objWriter->save($filename);

            $i++;
        }
        $phpSecondExcel = new PHPExcel();
        $i2 = 2;
        foreach ($imageId as $skk => $svv) {
            $sortId = 0;
            array_shift($svv);
            foreach ($svv as $k2 => $v2) {
                $phpSecondExcel->getActiveSheet()->setCellValueExplicit('A' . $i2, strval($skk), PHPExcel_Cell_DataType::TYPE_STRING);
                $phpSecondExcel->getActiveSheet()->setCellValueExplicit('B' . $i2, $v2, PHPExcel_Cell_DataType::TYPE_STRING);
                $phpSecondExcel->getActiveSheet()->setCellValue('C' . $i2, $sortId);
                $i2++;
                $sortId++;
            }
            $pageSec = $phpSecondExcel->setActiveSheetIndex();
            $pageSec->setTitle('additionImg');
            $objWriter = PHPExcel_IOFactory::CreateWriter($phpSecondExcel, 'Excel2007');
            $filenamesc = 'additionImg.xlsx';
            $objWriter->save($filenamesc);
        }
        $i3 = 2;
        $phpThirdExcel = new PHPExcel();
        foreach ($getDb as $thirdk => $thirdv) {
            //print_r($thirdv);
            $phpThirdExcel->getActiveSheet()->setCellValueExplicit('A' . $i3, strval($thirdk), PHPExcel_Cell_DataType::TYPE_STRING);
            $phpThirdExcel->getActiveSheet()->setCellValue('B' . $i3, 0);
            $phpThirdExcel->getActiveSheet()->setCellValueExplicit('C' . $i3, $thirdv['keyword'], PHPExcel_Cell_DataType::TYPE_STRING);
            $pageTh = $phpThirdExcel->setActiveSheetIndex();
            $pageTh->setTitle('keywords');
            $objWriter = PHPExcel_IOFactory::CreateWriter($phpThirdExcel, 'Excel2007');
            $filenameth = 'keywords.xlsx';
            $objWriter->save($filenameth);

            $i3++;
        }
    }
}

/*Parser::setMainCategories();
Parser::setSubCategories('https://www.robik-music.com/muzykaljnye-instrumenty', 1, 7);
Parser::setSubCategories('https://www.robik-music.com/zvukovoe-oborudovanie', 2, 162);
Parser::setSubCategories('https://www.robik-music.com/svetovoe-oborudovanie', 3, 212);
Parser::setSubCategories('https://www.robik-music.com/dj-oborudovanie', 4, 231);
Parser::setSubCategories('https://www.robik-music.com/studijnoe-oborudovanie', 5, 244);
Parser::setSubCategories('https://www.robik-music.com/kabeljnaja-produkcija', 6, 261);
Parser::setSubCategories('https://www.robik-music.com/scena', 7, 275);*/

//Parser::getProduct('https://www.robik-music.com/muzykaljnye-instrumenty');
//Parser::getProduct('https://www.robik-music.com/zvukovoe-oborudovanie');
//Parser::getProduct('https://www.robik-music.com/svetovoe-oborudovanie');
//Parser::getProduct('https://www.robik-music.com/dj-oborudovanie');
//Parser::getProduct('https://www.robik-music.com/studijnoe-oborudovanie');
//Parser::getProduct('https://www.robik-music.com/kabeljnaja-produkcija');
//Parser::getProduct('https://www.robik-music.com/scena');


Parser::sqlToExcel('products');
//Parser::imageDownload();